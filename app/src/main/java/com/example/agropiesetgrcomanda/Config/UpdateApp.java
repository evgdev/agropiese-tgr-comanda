package com.example.agropiesetgrcomanda.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.example.agropiesetgrcomanda.Activities.ComandaMarfa;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkGetRequest;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.example.agropiesetgrcomanda.BuildConfig.APPLICATION_ID;

public class UpdateApp extends AsyncTask<String, Integer, Boolean> {
    String PATH;

    Activity activity;
    ProgressDialog dialog;

    public UpdateApp(Activity activity){
        this.activity = activity;
        dialog = new ProgressDialog(activity);
        dialog.setTitle(R.string.update_title);
        dialog.setMessage(ComandaMarfa.getAppInstance().getResources().getString(R.string.downloading_updates));
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setMax(100);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    @Override
    protected void onPreExecute(){
        this.dialog.show();
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        Boolean flag = null;
        PATH = Environment.getExternalStorageDirectory() + "/Download/";

        try {
                flag = false;

                String url = "https://api.agro.md/tab/tableta.apk";

                try {
                    NetworkGetRequest networkGetRequest = new NetworkGetRequest();
                    networkGetRequest.run(url, new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if(!response.isSuccessful()){
                                throw new IOException(ComandaMarfa.getAppInstance().getResources().getString(R.string.failure_code) + response);
                            } else {
                                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
                                File file = new File(PATH);
                                file.mkdirs();

                                File outputFile = new File(file, "tableta.apk");

                                if(outputFile.exists()){
                                    outputFile.delete();
                                }

                                InputStream is = response.body().byteStream();

                                int total_size = (int) response.body().contentLength();

                                byte[] buffer = new byte[1024];
                                int len1 = 0;
                                int per = 0;
                                int downloaded = 0;

                                FileOutputStream fos = new FileOutputStream(outputFile);

                                while ((len1 = is.read(buffer)) !=-1){
                                    fos.write(buffer, 0, len1);
                                    downloaded += len1;
                                    per = (downloaded * 100 /total_size);
                                    publishProgress(per);

                                }

                                fos.close();
                                is.close();
								if (dialog.isShowing()){
									dialog.dismiss();
								}
                                OpenNewVersion(PATH);
                            }
                        }
                    });
                } catch (Exception e){
                    e.printStackTrace();
                }

        } catch (Exception e){
            Log.e("JARIK", "Update: Update error! " + e.getMessage());
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    protected void onProgressUpdate(Integer... values){
        this.dialog.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean){
        super.onPostExecute(aBoolean);
    }

    private void OpenNewVersion(String location) {

        Uri uri = FileProvider.getUriForFile(ComandaMarfa.getAppInstance().getApplicationContext(), APPLICATION_ID + ".fileprovider", new File(location + "tableta.apk"));

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ComandaMarfa.getAppInstance().startActivity(intent);
    }
}
