package com.example.agropiesetgrcomanda.Config;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import com.example.agropiesetgrcomanda.Adapter.CustomProductsList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class MainApp extends Application {

    private JSONArray params = new JSONArray();
    private String keyStr, selectedSediu, from = "", codMarfa,
            denMarfa, priceProduct, cantProduct, sediuID,
            fiscalCode, clientName, clientPhone, comment,
            priceID, nrDoc, lang, productCode;

    public static JSONObject preferences;

    private JSONConfig jsonConfig = new JSONConfig();

    private CustomProductsList adapter;

    public String getKeyStr(){
    return keyStr;
    }

    public void setKeyStr(String keyStr) {
    this.keyStr = keyStr;
    }

    public String getSelectedSediu() {
    return selectedSediu;
    }

    public void setSelectedSediu(String selectedSediu) {
    this.selectedSediu = selectedSediu;
    }

    public String getFrom() {
      return from;
    }

    public void setFrom(String from) {
      this.from = from;
    }

    public String getCodMarfa() {
      return codMarfa;
    }

    public void setCodMarfa(String codMarfa) {
        this.codMarfa = codMarfa;
    }

    public String getDenMarfa() {
        return denMarfa;
    }

    public void setDenMarfa(String denMarfa) {
        this.denMarfa = denMarfa;
    }

    public String getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(String priceProduct) {
        this.priceProduct = priceProduct;
    }

    public String getCantProduct() {
        return cantProduct;
    }

    public void setCantProduct(String cantProduct) {
        this.cantProduct = cantProduct;
    }

    public JSONObject getPreferences(){
      if(this.preferences == null){
          this.preferences = this.jsonConfig.readObject();
      }
      return this.preferences;
    }

    public String getPreferencesString(String name, String default_value){
      if(this.preferences == null){
          this.preferences = this.jsonConfig.readObject();
      }
      try {
          return this.preferences.getString(name);
      } catch (JSONException e){
          e.printStackTrace();
      } catch (NullPointerException e){
          e.printStackTrace();
      }
      return default_value;
    }

    public JSONObject updatePreferences(JSONObject preferences){
      this.jsonConfig.writeObject(preferences);
      this.preferences = this.jsonConfig.readObject();
      return this.preferences;
    }

    public JSONArray getOrder() {
        return params;
    }

    public void addOrder(JSONObject orderObj){
        this.params.put(orderObj);
    }

    public String getSediuID() {
        return sediuID;
    }

    public void setSediuID(String sediuID) {
        this.sediuID = sediuID;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public String getComment() {
        return comment;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPriceID() {
        return priceID;
    }

    public void setPriceID(String priceID) {
        this.priceID = priceID;
    }

    public String getNrDoc() {
        return nrDoc;
    }

    public void setNrDoc(String nrDoc) {
        this.nrDoc = nrDoc;
    }

    public String getLang(){
        return lang;
    }

    public void setLang(String lang){
        this.lang = lang;
    }

    public void setProductAdapter(CustomProductsList adapter){
        this.adapter = adapter;
    }

    public CustomProductsList getProductAdapter(){
        return adapter;
    }

    public void setProductCode(String productCode){
        this.productCode = productCode;
    }

    public String getProductCode(){
        return productCode;
    }

    public static void setLocale(String lang, Context context) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public static String currentLanguage(Context context){
        try{
            return preferences.getString("lang");
        }catch (Exception ex){
            ex.printStackTrace();
            return "ru";
        }
    }

    public boolean isOnline() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
