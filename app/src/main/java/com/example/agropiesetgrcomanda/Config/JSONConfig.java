package com.example.agropiesetgrcomanda.Config;

import com.example.agropiesetgrcomanda.Activities.ComandaMarfa;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class JSONConfig {
    private FileInputStream fileIn;
    private FileOutputStream fileOut;
    private ObjectInputStream objectIn;
    private ObjectOutputStream objectOut;
    private Object outputObject;
    private String filePath;

    public JSONObject readObject() {
        JSONObject resp = null;

        try {
            filePath = ComandaMarfa.getAppInstance().getApplicationContext().getFilesDir().getAbsolutePath() + "/config.json";
            fileIn = new FileInputStream(filePath);
            objectIn = new ObjectInputStream(fileIn);
            outputObject = objectIn.readUTF();
            resp = new JSONObject(outputObject.toString());
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return resp;
        } catch (JSONException e){
            e.printStackTrace();
            return resp;
        }
        finally {
            if(objectIn != null){
                try {
                    objectIn.close();
                    fileIn.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return resp;
    }

    public void writeObject(JSONObject inputObject) {
        try {
            filePath = ComandaMarfa.getAppInstance().getApplicationContext().getFilesDir().getAbsolutePath() + "/config.json";
            fileOut = new FileOutputStream(filePath);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeUTF(inputObject.toString());
            fileOut.getFD().sync();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(objectOut != null){
                try {
                    objectOut.close();
                    fileOut.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
