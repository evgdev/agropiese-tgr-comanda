package com.example.agropiesetgrcomanda.Config;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

public class BaseAppCompatActivity extends AppCompatActivity{

	public JSONObject preferences;
	private FileInputStream fileIn;
	private FileOutputStream fileOut;
	private ObjectInputStream objectIn;
	private ObjectOutputStream objectOut;
	private Object outputObject;
	private String filePath;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		JSONObject resp = null;

		try {
			filePath = this.getFilesDir().getAbsolutePath() + "/config.json";
			fileIn = new FileInputStream(filePath);
			objectIn = new ObjectInputStream(fileIn);
			outputObject = objectIn.readUTF();
			this.preferences = new JSONObject(outputObject.toString());
			MainApp.setLocale(this.getPreferencesString("lang", "ru"), this );
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e){
			e.printStackTrace();
		}
		finally {
			if(objectIn != null){
				try {
					objectIn.close();
					fileIn.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}

		super.onCreate(savedInstanceState);


	}

	public String getPreferencesString(String name, String default_value){
		if(this.preferences == null){
			return default_value;
		}
		try {
			return this.preferences.getString(name);
		} catch (JSONException e){
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return default_value;
	}
}
