package com.example.agropiesetgrcomanda.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.agropiesetgrcomanda.Model.LanguageItem;
import com.example.agropiesetgrcomanda.R;

import java.util.ArrayList;

public class LanguageAdapter extends ArrayAdapter<LanguageItem>{

	public LanguageAdapter(Context context, ArrayList<LanguageItem> languageList){
		super(context, 0, languageList);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent){
		return initView(position, view, parent);
	}

	@Override
	public View getDropDownView(int position, View view, ViewGroup parent){
		return initView(position, view, parent);
	}

	private View initView(int position, View view, ViewGroup parent){
		if (view == null){
			view = LayoutInflater.from(getContext()).inflate(R.layout.langs_spinner_item, parent, false);
		}
		TextView langsTxt = view.findViewById(R.id.langsTXT);

		LanguageItem item = getItem(position);

		if (item != null){
			langsTxt.setText(item.getLanguage());
		}

		return view;
	}
}
