package com.example.agropiesetgrcomanda.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.agropiesetgrcomanda.R;


public class CustomProductsList extends ArrayAdapter<String> {

    private final Context context;
    private final String[] productID, productDen, priceID, productPrice;
    public View view;

    public CustomProductsList(Context context, String[] productID, String productDen[], String[] priceID, String[] productPrice){
        super(context, -1, productID);
        this.context = context;
        this.productID = productID;
        this.productDen = productDen;
        this.priceID = priceID;
        this.productPrice = productPrice;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.custom_products_list, parent, false);

        TextView id = view.findViewById(R.id.productID);
        TextView den = view.findViewById(R.id.productDen);
        TextView priceId = view.findViewById(R.id.priceID);
        TextView price = view.findViewById(R.id.productPrice);

        id.setText(productID[position]);
        den.setText(productDen[position]);
        priceId.setText(priceID[position]);
        price.setText(productPrice[position]);

        return view;
    }
}
