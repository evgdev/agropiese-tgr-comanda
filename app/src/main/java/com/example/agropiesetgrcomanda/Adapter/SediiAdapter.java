package com.example.agropiesetgrcomanda.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Model.SediiItemModel;

import java.util.ArrayList;

public class SediiAdapter extends ArrayAdapter{

	public SediiAdapter(Context context, ArrayList<SediiItemModel> arrayList){
		super(context, 0, arrayList);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent){
		return initView(position, view, parent);
	}

	@Override
	public View getDropDownView(int position, View view, ViewGroup parent){
		return initView(position, view, parent);
	}

	private View initView(int position, View view, ViewGroup parent){
		if (view == null){
			view = LayoutInflater.from(getContext()).inflate(R.layout.sedii_spinner_item, parent, false);
		}

		TextView numeSediu = view.findViewById(R.id.sediuTXT);
		TextView sediuID = view.findViewById(R.id.sediuID);

		SediiItemModel item = (SediiItemModel) getItem(position);

		if (item != null){
			numeSediu.setText(item.getSediu());
			sediuID.setText(item.getSediuID());
		}

		return view;
	}
}
