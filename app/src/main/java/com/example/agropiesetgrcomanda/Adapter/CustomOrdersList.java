package com.example.agropiesetgrcomanda.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.agropiesetgrcomanda.Model.OrderListModel;
import com.example.agropiesetgrcomanda.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CustomOrdersList extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<OrderListModel> modelList;
    ArrayList<OrderListModel> arrayList;

    public CustomOrdersList(Context context, List<OrderListModel> modelList){
        this.context = context;
        this.modelList = modelList;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(modelList);
        inflater = LayoutInflater.from(context);
    }

    public class ViewHolder{
        TextView denMarfa, cantMarfa, managerName, dateOrder, clientName, statusOrder, lastModOrder, orderId, codMarfa;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        ViewHolder holder;
        if(v == null){
            holder = new ViewHolder();

            v = inflater.inflate(R.layout.custom_orders_list, null);

            holder.denMarfa = v.findViewById(R.id.denMarfa);
            holder.cantMarfa = v.findViewById(R.id.managerName);
            holder.managerName = v.findViewById(R.id.dateOrder);
            holder.dateOrder = v.findViewById(R.id.lastModOrder);
            holder.clientName = v.findViewById(R.id.cantMarfa);
            holder.statusOrder = v.findViewById(R.id.statusOrder);
            holder.lastModOrder = v.findViewById(R.id.clientName);
            holder.orderId = v.findViewById(R.id.orderId);
            holder.codMarfa = v.findViewById(R.id.codMarfa);

            v.setTag(holder);

        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.denMarfa.setText(modelList.get(position).getProductDen());
        holder.cantMarfa.setText(modelList.get(position).getManager());
        holder.managerName.setText(modelList.get(position).getOrderDate());
        holder.dateOrder.setText(modelList.get(position).getLastModOrder());
        holder.clientName.setText(modelList.get(position).getProductCant());
        holder.statusOrder.setText(modelList.get(position).getOrderStatus());
        holder.lastModOrder.setText(modelList.get(position).getClientName());
        holder.orderId.setText(modelList.get(position).getOrderID());
        holder.codMarfa.setText(modelList.get(position).getCodMarfa());

        return v;
    }

    public void filter(String text){

        text = text.toLowerCase(Locale.getDefault());
        modelList.clear();
        if(text.length() == 0) {
            modelList.addAll(arrayList);
        } else {
            for( OrderListModel model : arrayList) {
                if(model.getClientName().toLowerCase(Locale.getDefault()).contains(text)){
                    modelList.add(model);
                } else if(model.getProductDen().toLowerCase(Locale.getDefault()).contains(text)){
                    modelList.add(model);
                } else if (model.getCodMarfa().toLowerCase(Locale.getDefault()).contains(text)){
                    modelList.add(model);
                }
            }
        }
        notifyDataSetChanged();

    }
}
