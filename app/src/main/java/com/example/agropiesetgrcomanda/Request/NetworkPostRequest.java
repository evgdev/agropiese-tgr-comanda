package com.example.agropiesetgrcomanda.Request;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;

public class NetworkPostRequest {

    OkHttpClient client = new OkHttpClient.Builder()
            .protocols(Arrays.asList(Protocol.HTTP_1_1))
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public Call run(String url, FormBody rb, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .post(rb)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }
    
}
