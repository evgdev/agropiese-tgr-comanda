package com.example.agropiesetgrcomanda.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.example.agropiesetgrcomanda.Adapter.LanguageAdapter;
import com.example.agropiesetgrcomanda.Adapter.SediiAdapter;
import com.example.agropiesetgrcomanda.Config.BaseAppCompatActivity;
import com.example.agropiesetgrcomanda.Config.MainApp;
import com.example.agropiesetgrcomanda.Config.UpdateApp;
import com.example.agropiesetgrcomanda.Model.LanguageItem;
import com.example.agropiesetgrcomanda.Model.SediiItemModel;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkGetRequest;
import com.example.agropiesetgrcomanda.Request.NetworkPostRequest;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainActivity extends BaseAppCompatActivity{

    private ArrayList<SediiItemModel> listSedii;
    private SediiAdapter adapter;
    private JSONObject preferences;

    public static String TAG = "TAG";

    Context context;

    Toolbar toolbar;
    ImageButton list;
    MainApp MA;

    ProgressDialog loading, check_keyDialog;

    EditText key;

    Spinner sedii, langs;

    Button btnSave;

    public ProgressDialog downloadUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        toolbar = findViewById(R.id.settingsToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        MA = (MainApp) getApplication();

        if (MA.isOnline()){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkPermission()){

            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        try{

            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            final int versionCode = packageInfo.versionCode;
            String versionName = packageInfo.versionName;

            String url = getString(R.string.serverVersion);

            NetworkGetRequest networkGetRequest = new NetworkGetRequest();
            networkGetRequest.run(url, new Callback(){
                @Override
                public void onFailure(Call call, IOException e){
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException{
                    if (!response.isSuccessful()){
                        throw new IOException(getString(R.string.failure_code) + response);
                    } else {

                        final int serverVersionCode = Integer.parseInt(response.body().string());

                        runOnUiThread(new Runnable(){
                            @Override
                            public void run(){
                                if (versionCode < serverVersionCode){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                            .setTitle(getResources().getString(R.string.update_title))
                                            .setMessage(getResources().getString(R.string.update_message))
                                            .setCancelable(false)
                                            .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                                                @Override
                                                public void onClick(DialogInterface dialog, int which){
                                                    dialog.dismiss();
                                                }
                                            })
                                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                                                @Override
                                                public void onClick(DialogInterface dialog, int which){
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                                                        UpdateApp updater = new UpdateApp(MainActivity.this);
                                                        updater.execute();
                                                    } else {
                                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.apkLink)));
                                                        startActivity(intent);
                                                    }
                                                }
                                            });

                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                        });
                    }
                }
            });
        } catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        loading = new ProgressDialog(MainActivity.this);
        loading.setTitle(getResources().getString(R.string.branches_title));
        loading.setMessage(getResources().getString(R.string.branches_loading));
        loading.setCancelable(false);
        loading.show();

        sedii = findViewById(R.id.sedii);
        listSedii = new ArrayList<>();

        try{

            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(sedii);

            popupWindow.setHeight(1000);
            popupWindow.setVerticalOffset(150);

            Drawable spinnerDrawable = sedii.getBackground().getConstantState().newDrawable();

            spinnerDrawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                sedii.setBackground(spinnerDrawable);
            } else {
                sedii.setBackgroundDrawable(spinnerDrawable);
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        String url = getString(R.string.sedii);
        NetworkGetRequest networkGetRequest = new NetworkGetRequest();
        networkGetRequest.run(url, new Callback(){
            @Override
            public void onFailure(Call call, IOException e){
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException{
                if (!response.isSuccessful()){
                    throw new IOException(getString(R.string.failure_code) + response);
                } else{
                	JSONArray JSONArray_sedii = null;
                		try{
                			JSONArray_sedii = new JSONArray(response.body().string());
						} catch (JSONException e){
                			e.printStackTrace();
						}
					final JSONArray finalJSONArray_sedii = JSONArray_sedii;
					runOnUiThread(new Runnable(){
						@Override
						public void run(){
							try{
								for (int i = 0; i < finalJSONArray_sedii.length(); i++){
									JSONObject sediuObj = finalJSONArray_sedii.getJSONObject(i);
									String sediuID = sediuObj.getString("ID");
									String sediuName = sediuObj.getString("SALES_TERRYTORY_NAME");

									SediiItemModel itemModel = new SediiItemModel(sediuName, sediuID);
									listSedii.add(itemModel);
								}
								adapter = new SediiAdapter(context, listSedii);
								sedii.setAdapter(adapter);

								sedii.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
									@Override
									public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
										TextView sediu = view.findViewById(R.id.sediuTXT);
										TextView sediuIDTXT = view.findViewById(R.id.sediuID);
										String selectedSediu = sediu.getText().toString();
										String sediuID = sediuIDTXT.getText().toString();
										MA.setSelectedSediu(selectedSediu);
										MA.setSediuID(sediuID);
									}

									@Override
									public void onNothingSelected(AdapterView<?> parent){
									}
								});
								if (loading.isShowing()){
									loading.dismiss();
								}
							}catch (JSONException e){
								e.printStackTrace();
							}
						}
					});
				}
            }
        });
        langs = findViewById(R.id.langs);
        try{
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(langs);
            popupWindow.setVerticalOffset(150);
            Drawable spinnerDrawable = langs.getBackground().getConstantState().newDrawable();
            spinnerDrawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                langs.setBackground(spinnerDrawable);
            } else {
                langs.setBackgroundDrawable(spinnerDrawable);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        ArrayList<LanguageItem> langsList;
        LanguageAdapter langsAdapter;
        langsList = new ArrayList<>();
        langsList.add(new LanguageItem(getResources().getString(R.string.choose_lang)));
        langsList.add(new LanguageItem(getResources().getString(R.string.romanian)));
        langsList.add(new LanguageItem(getResources().getString(R.string.russian)));
        langsAdapter = new LanguageAdapter(MainActivity.this, langsList);
        langs.setAdapter(langsAdapter);
        langs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
                switch (position){
                    case 1 :
                        MA.setLocale("ro", context);
                        MA.setLang("ro");
                        break;
                    case 2 :
                        MA.setLocale("ru", context);
                        MA.setLang("ru");
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent){
            }
        });
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (MA.isOnline()){
                    String api_token = MA.getPreferencesString("api_token", null);
                    if (api_token != null){
                        if (langs.getSelectedItemPosition() == 0){
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.choose_language), Toast.LENGTH_LONG).show();
                        }else{
                            String keyStr = MA.getPreferencesString("key", null);
                            MA.setKeyStr(keyStr);
                            JSONObject settings = new JSONObject();
                            try{
                                settings.put("lang", MA.getLang());
                                settings.put("key", MA.getKeyStr());
                                settings.put("sediu", MA.getSelectedSediu());
                                settings.put("sediuID", MA.getSediuID());
                                settings.put("api_token", api_token);
                                preferences = MA.updatePreferences(settings);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(MainActivity.this, ComandaMarfa.class);
                            startActivity(intent);
                        }
                    }else{
                        key = findViewById(R.id.key);
                        if (key.getText().toString().isEmpty()){
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.no_key), Toast.LENGTH_LONG).show();
                        }else{
                            String stringKey = key.getText().toString();
                            MA.setKeyStr(stringKey);
                            String key = MA.getKeyStr();
                            String sediu = MA.getSelectedSediu();
                            check_keyDialog = new ProgressDialog(MainActivity.this);
                            check_keyDialog.setCancelable(false);
                            check_keyDialog.setTitle(getResources().getString(R.string.check_key_title));
                            check_keyDialog.setMessage(getResources().getString(R.string.check_key_msg));
                            check_keyDialog.show();
                            FormBody.Builder form = new FormBody.Builder().add("key", key).add("sediu", sediu);
                            String url = getString(R.string.check_key);
                            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                            networkPostRequest.run(url, form.build(), new Callback(){
                                @Override
                                public void onFailure(Call call, IOException e){
                                    e.printStackTrace();
                                }
                                @Override
                                public void onResponse(Call call, Response response) throws IOException{

                                    if (! response.isSuccessful()){
                                        if (response.code() == 429){
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this).setCancelable(true).setTitle(getResources().getString(R.string.invalid_key)).setMessage(getResources().getString(R.string.key_429)).setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which){
                                                            dialog.dismiss();
                                                            Toast.makeText(MainActivity.this, getResources().getString(R.string.thanks), Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                    AlertDialog alertDialog = builder.create();
                                                    alertDialog.show();
                                                }
                                            });
                                        }else if (response.code() == 401){
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){
                                                    if (check_keyDialog.isShowing()){
                                                        check_keyDialog.dismiss();
                                                    }
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this).setCancelable(true).setTitle(getResources().getString(R.string.invalid_key)).setMessage(getResources().getString(R.string.invalid_key_branch)).setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which){
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                    AlertDialog dialog = builder.create();
                                                    dialog.show();
                                                }
                                            });
                                        }else{
                                            throw new IOException(getString(R.string.failure_code) + response);
                                        }
                                    }else{
                                        try{
                                            JSONArray resp = new JSONArray(response.body().string());
                                            final JSONObject respObj = resp.getJSONObject(0);

                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){
                                                    try{
                                                        Boolean result = respObj.getBoolean("result");
                                                        if (result){
                                                            String api_token = respObj.getString("token");
                                                            JSONObject settings = new JSONObject();

                                                            if (langs.getSelectedItemPosition() == 0){
                                                                Toast.makeText(MainActivity.this, getResources().getString(R.string.choose_language), Toast.LENGTH_LONG).show();
                                                            }else{
                                                                try{

                                                                    settings.put("key", MA.getKeyStr());
                                                                    settings.put("sediu", MA.getSelectedSediu());
                                                                    settings.put("sediuID", MA.getSediuID());
                                                                    settings.put("api_token", api_token);
                                                                    settings.put("lang", MA.getLang());
                                                                    preferences = MA.updatePreferences(settings);

                                                                    if (check_keyDialog.isShowing()){
                                                                        check_keyDialog.dismiss();
                                                                    }

                                                                    Intent intent = new Intent(MainActivity.this, ComandaMarfa.class);
                                                                    MA.setFrom("from");
                                                                    startActivity(intent);

                                                                }catch (JSONException e){
                                                                    e.printStackTrace();
                                                                    Log.e("Error put settings", e.getMessage());
                                                                }
                                                            }

                                                        }else{
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this).setTitle(getResources().getString(R.string.invalid_key)).setMessage(getResources().getString(R.string.invalid_key_branch)).setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which){
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                            AlertDialog dialog = builder.create();
                                                            dialog.show();
                                                        }

                                                    }catch (JSONException e){
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });

                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
        }
    }
    private boolean checkPermission(){
        return (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE + Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission(String[] strings, int i) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode){
            case 1 :
                if(grantResults.length > 0){
                    boolean accept = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(accept){

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE + Manifest.permission.WRITE_EXTERNAL_STORAGE));
                            showMessageOKCancel(getResources().getString(R.string.need_permissions),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                            {
                                                requestPermission(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new  android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setTitle(getResources().getString(R.string.permissions))
                .setPositiveButton("Ok", okListener)
                .create()
                .show();
    }
}
