package com.example.agropiesetgrcomanda.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agropiesetgrcomanda.Adapter.CustomOrdersList;
import com.example.agropiesetgrcomanda.Config.BaseAppCompatActivity;
import com.example.agropiesetgrcomanda.Config.MainApp;
import com.example.agropiesetgrcomanda.Model.OrderListModel;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkPostRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Response;

public class OrdersListActivity extends BaseAppCompatActivity{

    MainApp MA;
    Toolbar orderToolbar;
    ListView listOrders;
    CustomOrdersList adapter;
    ArrayList<OrderListModel> arrayList = new ArrayList<>();
    SwipeRefreshLayout layoutList;
    String replacement, orderStatus, manager, repManager;
    ProgressDialog loading;
    ImageButton settings;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);

        orderToolbar = findViewById(R.id.orderToolbar);
        setSupportActionBar(orderToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        context = this;

        MA = (MainApp)getApplication();
        settings = findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersListActivity.this, MainActivity.class);
                MA.setFrom("from");
                startActivity(intent);
            }
        });

        layoutList = findViewById(R.id.layoutList);
        listOrders = findViewById(R.id.listOrders);

        loading = new ProgressDialog(OrdersListActivity.this);
        loading.setMessage(getResources().getString(R.string.loading_orders));
        loading.setCancelable(false);
        loading.show();

        final String url = getResources().getString(R.string.order_history);
        layoutList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                if (MA.isOnline()){
                    String api_token = MA.getPreferencesString("api_token", " ");
                    String fil_id = MA.getPreferencesString("sediuID", " ");
                    FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("fil_id", fil_id).add("status", 0 + "," + 1 + "," + "," + 3 + "," + 4);
                    NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                    networkPostRequest.run(url, form.build(), new Callback(){
                        @Override
                        public void onFailure(Call call, IOException e){
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, final Response response) throws IOException{
                            if (! response.isSuccessful()){
                                throw new IOException(getString(R.string.failure_code) + response);
                            }else{
                                JSONArray resp = null;
                                try{
                                    resp = new JSONArray(response.body().string());
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                                final JSONArray finalResp = resp;
                                runOnUiThread(new Runnable(){
                                    @Override
                                    public void run(){

                                        try{
                                            arrayList.clear();
                                            for (int i = 0; i < finalResp.length(); i++){

                                                JSONObject order = finalResp.getJSONObject(i);

                                                String[] productsDen = new String[finalResp.length()];
                                                String[] productsCant = new String[finalResp.length()];
                                                String[] managers = new String[finalResp.length()];
                                                String[] ordersDate = new String[finalResp.length()];
                                                String[] lastModOrders = new String[finalResp.length()];
                                                String[] clientNames = new String[finalResp.length()];
                                                String[] ordersStatus = new String[finalResp.length()];
                                                String[] ordersID = new String[finalResp.length()];
                                                String[] codsMarfa = new String[finalResp.length()];

                                                String denMarfa = order.getString("DEN_MARFA");
                                                String cantMarfa = order.getString("CANT_MARFA");
                                                int cantMArfaInt = Math.round(Float.parseFloat(cantMarfa));
                                                cantMarfa = String.valueOf(cantMArfaInt);
                                                manager = order.getString("MANAGER_KEY");
                                                if (manager.equals("null")){
                                                    repManager = MA.getPreferencesString("sediu", "");
                                                }else{
                                                    repManager = manager;
                                                }
                                                String dateOrder = order.getString("TCL_IDATA");
                                                String lastModOrder = order.getString("TCL_STATUS_MODIFICAT");
                                                String clientName = order.getString("TCL_DENCOMP");
                                                String orderID = order.getString("TCL_ID");
                                                String codMarfa = order.getString("COD_MARFA");
                                                orderStatus = order.getString("TCL_STATUS");
                                                switch (orderStatus){
                                                    case "0":
                                                        replacement = getResources().getString(R.string.new_order);
                                                        break;
                                                    case "1":
                                                        replacement = getResources().getString(R.string.processing_order);
                                                        break;
                                                    case "3":
                                                        replacement = getResources().getString(R.string.delivered_order);
                                                        break;
                                                    case "4":
                                                        replacement = getResources().getString(R.string.waiting_order);
                                                        break;
                                                    case "15":
                                                        replacement = getResources().getString(R.string.success_order);
                                                        break;
                                                    case "17":
                                                        replacement = getResources().getString(R.string.denied_client);
                                                        break;
                                                    case "16":
                                                        replacement = getResources().getString(R.string.denied_company);
                                                        break;
                                                    default:
                                                        replacement = getResources().getString(R.string.invalid_status);
                                                        break;
                                                }

                                                productsDen[i] = denMarfa;
                                                productsCant[i] = cantMarfa;
                                                managers[i] = repManager;
                                                ordersDate[i] = dateOrder;
                                                clientNames[i] = clientName;
                                                ordersStatus[i] = replacement;
                                                lastModOrders[i] = lastModOrder;
                                                ordersID[i] = orderID;
                                                codsMarfa[i] = codMarfa;
                                                codsMarfa[i] = codMarfa;

                                                OrderListModel model = new OrderListModel(productsDen[i], productsCant[i], managers[i], ordersDate[i], clientNames[i], ordersStatus[i], lastModOrders[i], ordersID[i], codsMarfa[i]);
                                                arrayList.add(model);

                                            }
                                            adapter = new CustomOrdersList(context, arrayList);
                                            listOrders.setAdapter(adapter);
                                            adapter.notifyDataSetChanged();
                                            if (layoutList.isRefreshing()){
                                                layoutList.setRefreshing(false);
                                            }
                                            listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, final int position, long id){

                                                    final TextView orderIDTXT = view.findViewById(R.id.orderId);
                                                    TextView orderStatusTXT = view.findViewById(R.id.statusOrder);
                                                    String orderStatusStr = orderStatusTXT.getText().toString();
                                                    if (orderStatusStr.equals(getResources().getString(R.string.new_order))){
                                                        orderStatus = "0";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.processing_order))){
                                                        orderStatus = "1";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.delivered_order))){
                                                        orderStatus = "3";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.waiting_order))){
                                                        orderStatus = "4";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.success_order))){
                                                        orderStatus = "15";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.denied_company))){
                                                        orderStatus = "16";
                                                    }else if (orderStatusStr.equals(getResources().getString(R.string.denied_client))){
                                                        orderStatus = "17";
                                                    }else{
                                                        orderStatus = getResources().getString(R.string.invalid_status);
                                                    }
                                                    if (orderStatus.equals("3")){
                                                        AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersListActivity.this);
                                                        dialog.setMessage(getResources().getString(R.string.confirm_order));
                                                        dialog.setCancelable(true);
                                                        dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                                                            public void onClick(DialogInterface dialog, int id){
                                                                orderStatus = "15";
                                                                String orderID = orderIDTXT.getText().toString();
                                                                String api_token = MA.getPreferencesString("api_token", " ");
                                                                FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("order_id", orderID).add("status", orderStatus);

                                                                String url = getResources().getString(R.string.change_status);
                                                                NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                                                                networkPostRequest.run(url, form.build(), new Callback(){
                                                                    @Override
                                                                    public void onFailure(Call call, IOException e){
                                                                        e.printStackTrace();
                                                                    }

                                                                    @Override
                                                                    public void onResponse(Call call, Response response) throws IOException{
                                                                        if (! response.isSuccessful()){
                                                                            throw new IOException(getString(R.string.failure_code) + response);
                                                                        }else{

                                                                            runOnUiThread(new Runnable(){
                                                                                @Override
                                                                                public void run(){
                                                                                    Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.success_save_product), Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });

                                                            }
                                                        });

                                                        dialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                                                            public void onClick(DialogInterface dialog, int id){
                                                                dialog.cancel();
                                                            }
                                                        });

                                                        AlertDialog alert = dialog.create();
                                                        alert.show();
                                                    }else{
                                                        AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersListActivity.this);
                                                        dialog.setMessage(getResources().getString(R.string.delete_order));
                                                        dialog.setCancelable(true);
                                                        dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                                                            public void onClick(DialogInterface dialog, int id){
                                                                orderStatus = "17";
                                                                String orderID = orderIDTXT.getText().toString();
                                                                String api_token = MA.getPreferencesString("api_token", " ");
                                                                FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("order_id", orderID).add("status", orderStatus);

                                                                String url = getResources().getString(R.string.change_status);
                                                                NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                                                                networkPostRequest.run(url, form.build(), new Callback(){
                                                                    @Override
                                                                    public void onFailure(Call call, IOException e){
                                                                        e.printStackTrace();
                                                                    }

                                                                    @Override
                                                                    public void onResponse(Call call, final Response response) throws IOException{
                                                                        if (! response.isSuccessful()){
                                                                            throw new IOException(getString(R.string.failure_code) + response);
                                                                        }else{

                                                                            runOnUiThread(new Runnable(){
                                                                                @Override
                                                                                public void run(){
                                                                                    try{
                                                                                        JSONObject resp = new JSONObject(response.body().string());
                                                                                        Boolean result = resp.getBoolean("result");
                                                                                        if (result){
                                                                                            Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.success_delete_order), Toast.LENGTH_SHORT).show();
                                                                                        }else{
                                                                                            Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.denied_delete_order), Toast.LENGTH_SHORT).show();
                                                                                        }
                                                                                    }catch (JSONException e){
                                                                                        e.printStackTrace();
                                                                                    }catch (IOException e){
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });

                                                            }
                                                        });

                                                        dialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                                                            public void onClick(DialogInterface dialog, int id){
                                                                dialog.cancel();
                                                            }
                                                        });

                                                        AlertDialog alert = dialog.create();
                                                        alert.show();
                                                    }
                                                }
                                            });
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                    }
                                });
                            }

                        }
                    });
                } else {
                    Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (MA.isOnline()){
            String api_token = MA.getPreferencesString("api_token", " ");
            String fil_id = MA.getPreferencesString("sediuID", " ");
            FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("fil_id", fil_id).add("status", 0 + "," + 1 + "," + "," + 3 + "," + 4);
            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
            networkPostRequest.run(url, form.build(), new Callback(){
                @Override
                public void onFailure(Call call, IOException e){
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException{
                    if (! response.isSuccessful()){
                        runOnUiThread(new Runnable(){
                            @Override
                            public void run(){
                                if (response.code() == 401 || response.code() == 413 || response.code() == 429){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(OrdersListActivity.this);
                                    builder.setTitle(getResources().getString(R.string.invalid_key));
                                    builder.setMessage(getResources().getString(R.string.used_key));
                                    builder.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which){
                                            Intent intent = new Intent(OrdersListActivity.this, MainActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                        });
                        throw new IOException(getString(R.string.failure_code) + response);
                    }else{
                        JSONArray resp = null;
                        try{
                            resp = new JSONArray(response.body().string());
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                        final JSONArray finalResp = resp;
                        runOnUiThread(new Runnable(){
                            @Override
                            public void run(){

                                try{
                                    arrayList.clear();
                                    for (int i = 0; i < finalResp.length(); i++){

                                        JSONObject order = finalResp.getJSONObject(i);

                                        String[] productsDen = new String[finalResp.length()];
                                        String[] productsCant = new String[finalResp.length()];
                                        String[] managers = new String[finalResp.length()];
                                        String[] ordersDate = new String[finalResp.length()];
                                        String[] lastModOrders = new String[finalResp.length()];
                                        String[] clientNames = new String[finalResp.length()];
                                        String[] ordersStatus = new String[finalResp.length()];
                                        String[] ordersID = new String[finalResp.length()];
                                        String[] codsMarfa = new String[finalResp.length()];

                                        String denMarfa = order.getString("DEN_MARFA");
                                        String cantMarfa = order.getString("CANT_MARFA");
                                        int cantMArfaInt = Math.round(Float.parseFloat(cantMarfa));
                                        cantMarfa = String.valueOf(cantMArfaInt);
                                        String manager = order.getString("MANAGER_KEY");
                                        String dateOrder = order.getString("TCL_IDATA");
                                        String lastModOrder = order.getString("TCL_STATUS_MODIFICAT");
                                        String clientName = order.getString("TCL_DENCOMP");
                                        String orderID = order.getString("TCL_ID");
                                        String codMarfa = order.getString("COD_MARFA");
                                        if (manager.equals("null")){
                                            repManager = MA.getPreferencesString("sediu", "");
                                        }else{
                                            repManager = manager;
                                        }
                                        orderStatus = order.getString("TCL_STATUS");
                                        switch (orderStatus){
                                            case "0":
                                                replacement = getResources().getString(R.string.new_order);
                                                break;
                                            case "1":
                                                replacement = getResources().getString(R.string.processing_order);
                                                break;
                                            case "3":
                                                replacement = getResources().getString(R.string.delivered_order);
                                                break;
                                            case "4":
                                                replacement = getResources().getString(R.string.waiting_order);
                                                break;
                                            case "15":
                                                replacement = getResources().getString(R.string.success_order);
                                                break;
                                            case "17":
                                                replacement = getResources().getString(R.string.denied_client);
                                                break;
                                            case "16":
                                                replacement = getResources().getString(R.string.denied_company);
                                                break;
                                            default:
                                                replacement = getResources().getString(R.string.invalid_status);
                                                break;
                                        }

                                        productsDen[i] = denMarfa;
                                        productsCant[i] = cantMarfa;
                                        managers[i] = repManager;
                                        ordersDate[i] = dateOrder;
                                        clientNames[i] = clientName;
                                        ordersStatus[i] = replacement;
                                        lastModOrders[i] = lastModOrder;
                                        ordersID[i] = orderID;
                                        codsMarfa[i] = codMarfa;

                                        OrderListModel model = new OrderListModel(productsDen[i], productsCant[i], managers[i], ordersDate[i], clientNames[i], ordersStatus[i], lastModOrders[i], ordersID[i], codsMarfa[i]);
                                        arrayList.add(model);

                                    }
                                    adapter = new CustomOrdersList(context, arrayList);
                                    listOrders.setAdapter(adapter);
                                    if (loading.isShowing()){
                                        loading.dismiss();
                                    }
                                    listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id){
                                            if (MA.isOnline()){
                                                final TextView orderIDTXT = view.findViewById(R.id.orderId);
                                                TextView orderStatusTXT = view.findViewById(R.id.statusOrder);
                                                String orderStatusStr = orderStatusTXT.getText().toString();
                                                if (orderStatusStr.equals(getResources().getString(R.string.new_order))){
                                                    orderStatus = "0";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.processing_order))){
                                                    orderStatus = "1";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.delivered_order))){
                                                    orderStatus = "3";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.waiting_order))){
                                                    orderStatus = "4";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.success_order))){
                                                    orderStatus = "15";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.denied_company))){
                                                    orderStatus = "16";
                                                }else if (orderStatusStr.equals(getResources().getString(R.string.denied_client))){
                                                    orderStatus = "17";
                                                }else{
                                                    orderStatus = getResources().getString(R.string.invalid_status);
                                                }
                                                if (orderStatus.equals("3")){
                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersListActivity.this);
                                                    dialog.setMessage(getResources().getString(R.string.confirm_order));
                                                    dialog.setCancelable(true);
                                                    dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                                                        public void onClick(DialogInterface dialog, int id){
                                                            orderStatus = "15";
                                                            String orderID = orderIDTXT.getText().toString();
                                                            String api_token = MA.getPreferencesString("api_token", " ");
                                                            FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("order_id", orderID).add("status", orderStatus);

                                                            String url = getResources().getString(R.string.change_status);
                                                            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                                                            networkPostRequest.run(url, form.build(), new Callback(){
                                                                @Override
                                                                public void onFailure(Call call, IOException e){
                                                                    e.printStackTrace();
                                                                }

                                                                @Override
                                                                public void onResponse(Call call, Response response) throws IOException{
                                                                    if (! response.isSuccessful()){
                                                                        throw new IOException(getString(R.string.failure_code) + response);
                                                                    }else{

                                                                        runOnUiThread(new Runnable(){
                                                                            @Override
                                                                            public void run(){
                                                                                Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.success_delete_order), Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                        }
                                                    });

                                                    dialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                                                        public void onClick(DialogInterface dialog, int id){
                                                            dialog.cancel();
                                                        }
                                                    });

                                                    AlertDialog alert = dialog.create();
                                                    alert.show();
                                                }else{
                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersListActivity.this);
                                                    dialog.setMessage(getResources().getString(R.string.delete_order));
                                                    dialog.setCancelable(true);
                                                    dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                                                        public void onClick(DialogInterface dialog, int id){
                                                            orderStatus = "17";
                                                            String orderID = orderIDTXT.getText().toString();
                                                            String api_token = MA.getPreferencesString("api_token", " ");
                                                            FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("order_id", orderID).add("status", orderStatus);

                                                            String url = getResources().getString(R.string.change_status);
                                                            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                                                            networkPostRequest.run(url, form.build(), new Callback(){
                                                                @Override
                                                                public void onFailure(Call call, IOException e){
                                                                    e.printStackTrace();
                                                                }

                                                                @Override
                                                                public void onResponse(Call call, Response response) throws IOException{
                                                                    if (! response.isSuccessful()){
                                                                        throw new IOException(getString(R.string.failure_code) + response);
                                                                    }else{

                                                                        runOnUiThread(new Runnable(){
                                                                            @Override
                                                                            public void run(){
                                                                                Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.success_delete_order), Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                        }
                                                    });

                                                    dialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                                                        public void onClick(DialogInterface dialog, int id){
                                                            dialog.cancel();
                                                        }
                                                    });

                                                    AlertDialog alert = dialog.create();
                                                    alert.show();
                                                }
                                            } else {
                                                Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                                            }
                                        }
                                    });
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }

                            }
                        });
                    }

                }
            });
        } else {
            Toast.makeText(OrdersListActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem search = menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(TextUtils.isEmpty(s)){
                    adapter.filter("");
                    listOrders.clearTextFilter();
                } else {
                    adapter.filter(s);
                }
                return true;
            }
        });
        return true;
    }
}
