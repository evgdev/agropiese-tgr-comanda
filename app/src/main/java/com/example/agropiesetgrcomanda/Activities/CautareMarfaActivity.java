package com.example.agropiesetgrcomanda.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.example.agropiesetgrcomanda.Adapter.CustomProductsList;
import com.example.agropiesetgrcomanda.Config.BaseAppCompatActivity;
import com.example.agropiesetgrcomanda.Config.MainApp;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkPostRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Response;

public class CautareMarfaActivity extends BaseAppCompatActivity{

    Toolbar mainToolbar;
    ConstraintLayout topLayout;
    LinearLayout listProductsLayout;
    MainApp MA;
    ImageButton settings, list;
    Button searchProducts;
    EditText cautareMarfa;
    ProgressDialog loading;
    JSONObject respData;
    JSONArray productsArray;
    ListView listProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cautare_marfa);

        mainToolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        topLayout = findViewById(R.id.topLayout);

        settings = findViewById(R.id.settings);
        list = findViewById(R.id.list);
        searchProducts = findViewById(R.id.searchProducts);
        cautareMarfa = findViewById(R.id.cautareMarfa);
        listProducts = findViewById(R.id.listProducts);

        MA = (MainApp)getApplication();

        if (MA.getProductAdapter() != null){
            cautareMarfa.setText(MA.getProductCode());
            listProducts.setAdapter(MA.getProductAdapter());
        }

        cautareMarfa.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){
                if (MA.isOnline()){
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                        if (cautareMarfa.getText().length() < 3){
                            AlertDialog.Builder dialog = new AlertDialog.Builder(CautareMarfaActivity.this);
                            dialog.setTitle(getResources().getString(R.string.quantity));
                            dialog.setMessage(getResources().getString(R.string.min_char));
                            dialog.setCancelable(true);
                            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog alert = dialog.create();
                            alert.show();
                        }else{
                            InputMethodManager inputMethodManager = (InputMethodManager) CautareMarfaActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(CautareMarfaActivity.this.getCurrentFocus().getWindowToken(), 0);
                            MA.setProductCode(cautareMarfa.getText().toString());
                            loading = new ProgressDialog(CautareMarfaActivity.this);
                            loading.setTitle(getResources().getString(R.string.products));
                            loading.setMessage(getResources().getString(R.string.products_loading));
                            loading.setCancelable(false);
                            loading.show();
                            String api_token = MA.getPreferencesString("api_token", "");
                            FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("code", cautareMarfa.getText().toString());

                            String url = getResources().getString(R.string.search);
                            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                            networkPostRequest.run(url, form.build(), new Callback(){
                                @Override
                                public void onFailure(Call call, IOException e){
                                    e.printStackTrace();
                                    Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onResponse(Call call, final Response response) throws IOException{
                                    try{
                                        if (! response.isSuccessful()){
                                            if (response.code() == 404){
                                                runOnUiThread(new Runnable(){
                                                    @Override
                                                    public void run(){
                                                        if (loading.isShowing()){
                                                            loading.dismiss();
                                                        }
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(CautareMarfaActivity.this).setTitle(getResources().getString(R.string.invalid_code)).setMessage(getResources().getString(R.string.products_404)).setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which){
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        AlertDialog dialog = builder.create();
                                                        dialog.show();
                                                    }
                                                });
                                            }else if (response.code() == 429){
                                                runOnUiThread(new Runnable(){
                                                    @Override
                                                    public void run(){
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(CautareMarfaActivity.this);
                                                        builder.setTitle(getResources().getString(R.string.invalid_key));
                                                        builder.setMessage(getResources().getString(R.string.used_key));
                                                        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which){
                                                                Intent intent = new Intent(CautareMarfaActivity.this, MainActivity.class);
                                                                startActivity(intent);
                                                            }
                                                        });
                                                        AlertDialog dialog = builder.create();
                                                        dialog.show();
                                                    }
                                                });
                                            }else{
                                                throw new IOException(getString(R.string.failure_code) + response);
                                            }
                                        }else{
                                            try{

                                                respData = new JSONObject(response.body().string());
                                                productsArray = respData.getJSONArray("products");

                                            }catch (JSONException e){
                                                e.printStackTrace();
                                            }

                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){

                                                    String[] productsID = new String[productsArray.length()];
                                                    String[] productsDen = new String[productsArray.length()];
                                                    String[] pricesID = new String[productsArray.length()];
                                                    String[] productsPrice = new String[productsArray.length()];

                                                    try{

                                                        for (int i = 0; i < productsArray.length(); i++){

                                                            JSONObject products = productsArray.getJSONObject(i);
                                                            String codMarfa = products.getString("PR_COD");
                                                            String denMarfa = products.getString("PR_DES");
                                                            String priceId = products.getString("PR_IND");
                                                            String productPrice = products.getString("PR_PRET");

                                                            Float productPricefl = Float.parseFloat(productPrice);
                                                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                                                            decimalFormat.setMaximumFractionDigits(2);
                                                            productPrice = String.valueOf(productPricefl);

                                                            productsID[i] = codMarfa;
                                                            productsDen[i] = denMarfa;
                                                            pricesID[i] = priceId;
                                                            productsPrice[i] = productPrice;

                                                        }
                                                        CustomProductsList listitem = new CustomProductsList(CautareMarfaActivity.this, productsID, productsDen, pricesID, productsPrice);
                                                        listProducts.setAdapter(listitem);
                                                        MA.setProductAdapter(listitem);
                                                        if (loading.isShowing()){
                                                            loading.dismiss();
                                                        }
                                                        if (listitem.getCount() == 0){
                                                            AlertDialog.Builder alert = new AlertDialog.Builder(CautareMarfaActivity.this);
                                                            alert.setTitle(getResources().getString(R.string.failure_search));
                                                            alert.setMessage(getResources().getString(R.string.products_404));
                                                            alert.setNegativeButton("Ok", new DialogInterface.OnClickListener(){
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which){
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                            alert.setCancelable(false);
                                                            alert.show();
                                                        }
                                                    }catch (JSONException e){
                                                        e.printStackTrace();
                                                    }

                                                }
                                            });

                                        }
                                    }catch (final IOException e){
                                        runOnUiThread(new Runnable(){
                                            @Override
                                            public void run(){
                                                Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        return true;
                    }
                } else {
                    Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                }
                return CautareMarfaActivity.super.onKeyDown(keyCode, event);
            }
        });

        listProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView priceID = view.findViewById(R.id.priceID);
                TextView priceTxT = view.findViewById(R.id.productPrice);
                String productPriceID = priceID.getText().toString();
                String price = priceTxT.getText().toString();

                Intent intent = new Intent(CautareMarfaActivity.this, AdaugareMarfaActivity.class);
                intent.putExtra("priceID", productPriceID);
                intent.putExtra("price", price);
                startActivity(intent);

            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MA.isOnline()){
                    Intent intent = new Intent(CautareMarfaActivity.this, MainActivity.class);
                    MA.setFrom("from");
                    startActivity(intent);
                } else {
                    Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                }
            }
        });

        searchProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MA.isOnline()){
                    if (cautareMarfa.getText().length() < 3){
                        AlertDialog.Builder dialog = new AlertDialog.Builder(CautareMarfaActivity.this);
                        dialog.setTitle(getResources().getString(R.string.quantity));
                        dialog.setMessage(getResources().getString(R.string.min_char));
                        dialog.setCancelable(true);
                        dialog.setNeutralButton("OK", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.show();
                    }else{
                        InputMethodManager inputMethodManager = (InputMethodManager) CautareMarfaActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(CautareMarfaActivity.this.getCurrentFocus().getWindowToken(), 0);
                        loading = new ProgressDialog(CautareMarfaActivity.this);
                        loading.setTitle(getResources().getString(R.string.products));
                        loading.setMessage(getResources().getString(R.string.products_loading));
                        loading.setCancelable(false);
                        loading.show();
                        String api_token = MA.getPreferencesString("api_token", "");
                        FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("code", cautareMarfa.getText().toString());

                        String url = getResources().getString(R.string.search);
                        NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                        networkPostRequest.run(url, form.build(), new Callback(){
                            @Override
                            public void onFailure(Call call, IOException e){
                                e.printStackTrace();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException{
                                try{
                                    if (! response.isSuccessful()){
                                        if (response.code() == 404){
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){
                                                    if (loading.isShowing()){
                                                        loading.dismiss();
                                                    }
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(CautareMarfaActivity.this).setTitle(getResources().getString(R.string.invalid_code)).setMessage(getResources().getString(R.string.products_404)).setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which){
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                    AlertDialog dialog = builder.create();
                                                    dialog.show();
                                                }
                                            });
                                        }else if (response.code() == 429){
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run(){
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(CautareMarfaActivity.this);
                                                    builder.setTitle(getResources().getString(R.string.invalid_key));
                                                    builder.setMessage(getResources().getString(R.string.used_key));
                                                    builder.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which){
                                                            Intent intent = new Intent(CautareMarfaActivity.this, MainActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    });
                                                    AlertDialog dialog = builder.create();
                                                    dialog.show();
                                                }
                                            });
                                        }else{
                                            throw new IOException(getString(R.string.failure_code) + response);
                                        }
                                    }else{
                                        try{

                                            respData = new JSONObject(response.body().string());
                                            productsArray = respData.getJSONArray("products");

                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                        runOnUiThread(new Runnable(){
                                            @Override
                                            public void run(){

                                                String[] productsID = new String[productsArray.length()];
                                                String[] productsDen = new String[productsArray.length()];
                                                String[] pricesID = new String[productsArray.length()];
                                                String[] productsPrice = new String[productsArray.length()];

                                                try{

                                                    for (int i = 0; i < productsArray.length(); i++){

                                                        JSONObject products = productsArray.getJSONObject(i);
                                                        String codMarfa = products.getString("PR_COD");
                                                        String denMarfa = products.getString("PR_DES");
                                                        String priceId = products.getString("PR_IND");
                                                        String productPrice = products.getString("PR_PRET");

                                                        Float productPricefl = Float.parseFloat(productPrice);
                                                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                                                        decimalFormat.setMaximumFractionDigits(2);
                                                        productPrice = String.valueOf(productPricefl);

                                                        productsID[i] = codMarfa;
                                                        productsDen[i] = denMarfa;
                                                        pricesID[i] = priceId;
                                                        productsPrice[i] = productPrice;

                                                    }
                                                    CustomProductsList listitem = new CustomProductsList(CautareMarfaActivity.this, productsID, productsDen, pricesID, productsPrice);
                                                    listProducts.setAdapter(listitem);
                                                    MA.setProductAdapter(listitem);
                                                    if (loading.isShowing()){
                                                        loading.dismiss();
                                                    }
                                                    if (listitem.getCount() == 0){
                                                        AlertDialog.Builder alert = new AlertDialog.Builder(CautareMarfaActivity.this);
                                                        alert.setTitle(getResources().getString(R.string.products));
                                                        alert.setMessage(getResources().getString(R.string.products_404));
                                                        alert.setNegativeButton("Ok", new DialogInterface.OnClickListener(){
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which){
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        alert.setCancelable(false);
                                                        alert.show();
                                                    }

                                                }catch (JSONException e){
                                                    e.printStackTrace();
                                                }

                                            }
                                        });

                                    }
                                }catch (final IOException e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MA.isOnline()){
                    Intent intent = new Intent(CautareMarfaActivity.this, OrdersListActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(CautareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                }
            }
        });

    }
}
