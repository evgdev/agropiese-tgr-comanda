package com.example.agropiesetgrcomanda.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agropiesetgrcomanda.Config.BaseAppCompatActivity;
import com.example.agropiesetgrcomanda.Config.MainApp;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkPostRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Response;

public class AdaugareMarfaActivity extends BaseAppCompatActivity{

    MainApp MA;
    Toolbar mainToolbar;
    ImageButton settings, list;
    ImageView productImg;
    Button btnSave, btnShowStock;
    EditText setCant;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adaugare_marfa);

        mainToolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        MA = (MainApp)getApplication();
        settings = findViewById(R.id.settings);
        list = findViewById(R.id.list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setCant = findViewById(R.id.setCant);
        btnSave = findViewById(R.id.btnSave);
        productImg = findViewById(R.id.productImg);
        productImg.setVisibility(View.GONE);

        if (MA.isOnline()){

        final TextView stocuri = findViewById(R.id.stocuri);

        final Bundle data = getIntent().getExtras();
        final String priceID = data.getString("priceID");
        final String price = data.getString("price");
        MA.setPriceProduct(price);
        MA.setPriceID(priceID);

        loading = new ProgressDialog(AdaugareMarfaActivity.this);
        loading.setMessage(getResources().getString(R.string.stock_loading));
        loading.setCancelable(false);
        loading.show();

        Picasso.with(AdaugareMarfaActivity.this)
                .load("https://agropiese.md/ImgDetail/" + priceID + ".jpeg")
                .into(productImg, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        productImg.setVisibility(View.VISIBLE);
                        if (loading.isShowing()){
                            loading.dismiss();
                        }

                    }

                    @Override
                    public void onError() {}
                });

        final String api_token = MA.getPreferencesString("api_token", " ");
        String url = getResources().getString(R.string.stock);
        FormBody.Builder formBody = new FormBody.Builder()
                .add("api_token", api_token)
                .add("ind", priceID);

        NetworkPostRequest networkPostRequest = new NetworkPostRequest();
        networkPostRequest.run(url, formBody.build(), new Callback(){
            @Override
            public void onFailure(Call call, IOException e){
                e.printStackTrace();
                Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException{
                try{
                    if (!response.isSuccessful()){
                        throw new IOException(getString(R.string.failure_code) + response);
                    } else{
                        final JSONObject respData = new JSONObject(response.body().string());
                        runOnUiThread(new Runnable(){
                            @Override
                            public void run(){
                                try{

                                    JSONObject stocMarfa = respData.getJSONObject("data");
                                    String denMarfa = stocMarfa.getString("den_marfa");
                                    MA.setDenMarfa(denMarfa);
                                    String codMarfa = stocMarfa.getString("cod_marfa");
                                    MA.setCodMarfa(codMarfa);
                                    TextView codMarfaTxt = findViewById(R.id.codMarfa);
                                    TextView denMarfaTxt = findViewById(R.id.denMarfa);
                                    TextView pretMarfaTxt = findViewById(R.id.pretMarfa);
                                    Iterator<String> i = stocMarfa.keys();
                                    while (i.hasNext()){
                                        String key = i.next();
                                        if (key.startsWith("stoc_")){
                                            stocuri.setText(stocuri.getText().toString() + "\n"+key.replace("stoc_", "").toUpperCase()+ ": " + stocMarfa.getString(key));
                                        }
                                    }
                                    codMarfaTxt.setText(getResources().getString(R.string.product_code) + "\n" + codMarfa);
                                    denMarfaTxt.setText(getResources().getString(R.string.product_name) + "\n" + denMarfa);
                                    pretMarfaTxt.setText(getResources().getString(R.string.product_price) + "\n" + price);
                                    if (loading.isShowing()){
                                        loading.dismiss();
                                    }
                                } catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (final Exception e){
                    e.printStackTrace();
                    runOnUiThread(new Runnable(){
                        @Override
                        public void run(){
                            Log.e("TAG", e.getMessage());
                            Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MA.isOnline()){
                    Intent intent = new Intent(AdaugareMarfaActivity.this, MainActivity.class);
                    MA.setFrom("from");
                    startActivity(intent);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (MA.isOnline()){
                    JSONArray params = MA.getOrder();
                    JSONObject orderInfo = new JSONObject();
                    MA.setCantProduct(setCant.getText().toString());
                    String cant = MA.getCantProduct();
                    if (MA.getCantProduct().length() <= 0){
                        AlertDialog.Builder dialog = new AlertDialog.Builder(AdaugareMarfaActivity.this);
                        dialog.setTitle(getResources().getString(R.string.stock_title));
                        dialog.setMessage(getResources().getString(R.string.enter_quantity));
                        dialog.setCancelable(true);
                        dialog.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.show();
                    }else if (Integer.parseInt(cant) <= 0){
                        AlertDialog.Builder dialog = new AlertDialog.Builder(AdaugareMarfaActivity.this);
                        dialog.setMessage(getResources().getString(R.string.not_null_quantity));
                        dialog.setCancelable(true);
                        dialog.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.show();
                    }else{
                        {
                            String cantMarfaStr = MA.getCantProduct();
                            int cantMarfa = Integer.parseInt(cantMarfaStr);
                            String priceIdStr = MA.getPriceID();
                            int priceID = Integer.parseInt(priceIdStr);
                            String sediuIdStr = MA.getPreferencesString("sediuID", "");
                            int sediuID = Integer.parseInt(sediuIdStr);
                            String sediu = MA.getPreferencesString("sediu", "");
                            String keyStr = MA.getPreferencesString("key", "");
                            int key = Integer.parseInt(keyStr);
                            String clientPhone = MA.getClientPhone();
                            String clientName = MA.getClientName();
                            if (clientName.equals("")){
                                clientName = MA.getPreferencesString("sediu", "");
                            }
                            String fiscalCode = MA.getFiscalCode();
                            String comment = MA.getComment();
                            String nrDoc = MA.getNrDoc();

                            try{
                                orderInfo.put("sediu", sediu);
                                orderInfo.put("key", key);
                                orderInfo.put("fil_id", sediuID);
                                orderInfo.put("tel_client", clientPhone);
                                orderInfo.put("nume_client", clientName);
                                orderInfo.put("cod_fiscal", fiscalCode);
                                orderInfo.put("comment", comment);

                                JSONObject productInfoObj = new JSONObject();

                                productInfoObj.put("price_id", priceID);
                                productInfoObj.put("cant", cantMarfa);
                                orderInfo.put("marfa", productInfoObj);
                                params.put(orderInfo);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                            FormBody.Builder form = new FormBody.Builder().add("api_token", api_token).add("price_id", String.valueOf(priceID)).add("cant", String.valueOf(cantMarfa)).add("numar_document", nrDoc).add("cod_fiscal", fiscalCode).add("nume_client", clientName).add("tel_client", clientPhone).add("comment", comment).add("fil_id", String.valueOf(sediuID)).add("key", keyStr);

                            String url = getResources().getString(R.string.insert_order);

                            NetworkPostRequest networkPostRequest = new NetworkPostRequest();
                            networkPostRequest.run(url, form.build(), new Callback(){
                                @Override
                                public void onFailure(Call call, IOException e){
                                    e.printStackTrace();
                                    Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException{
                                    if (! response.isSuccessful()){
                                        throw new IOException(getString(R.string.failure_code) + response);
                                    }else{
                                        Boolean callback = null;

                                        try{
                                            JSONObject resp = new JSONObject(response.body().string());
                                            callback = resp.getBoolean("result");

                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                        final Boolean finalCallback = callback;
                                        runOnUiThread(new Runnable(){
                                            @Override
                                            public void run(){
                                                if (finalCallback == true){
                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(AdaugareMarfaActivity.this);
                                                    dialog.setTitle(getResources().getString(R.string.success_order));
                                                    dialog.setMessage(getResources().getString(R.string.success_save_product));
                                                    dialog.setCancelable(false);
                                                    dialog.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which){
                                                            Intent intent = new Intent(AdaugareMarfaActivity.this, CautareMarfaActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    });
                                                    AlertDialog alert = dialog.create();
                                                    alert.show();
                                                }else{
                                                    Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.failure_save_product), Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MA.isOnline()){
                    Intent intent = new Intent(AdaugareMarfaActivity.this, OrdersListActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                }
            }
        });

        btnShowStock = findViewById(R.id.btnOpenStock);
        btnShowStock.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(AdaugareMarfaActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.stocks_dialog, null);
                builder.setView(view);
                TextView stocks = view.findViewById(R.id.stocuri);
                stocks.setMovementMethod(new ScrollingMovementMethod());
                stocks.setText(stocuri.getText().toString());
                builder.setTitle(getResources().getString(R.string.stock_title));
                builder.setCancelable(true);
                builder.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        } else {
            Toast.makeText(AdaugareMarfaActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
        }

    }
}
