package com.example.agropiesetgrcomanda.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.agropiesetgrcomanda.Config.BaseAppCompatActivity;
import com.example.agropiesetgrcomanda.Config.MainApp;
import com.example.agropiesetgrcomanda.Config.UpdateApp;
import com.example.agropiesetgrcomanda.R;
import com.example.agropiesetgrcomanda.Request.NetworkGetRequest;
import com.example.agropiesetgrcomanda.Request.NetworkPostRequest;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ComandaMarfa extends BaseAppCompatActivity{

    EditText clientName, nrDoc, fisclaCode, clientPhone, comment;
    MainApp MA;
    Toolbar toolbar;
    Button search;
    ImageButton settings, list;
    Context context;
    public static ComandaMarfa appInstance;
	public static String TAG = "TAG";

	private JSONObject preferences;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_comanda_marfa);
        context = getApplicationContext();
		appInstance = this;
		MA = (MainApp) getApplication();
        toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        settings = findViewById(R.id.settings);
        list = findViewById(R.id.list);
        search = findViewById(R.id.search);

        if (MA.isOnline()){

			try{

				PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
				final int versionCode = packageInfo.versionCode;
				String versionName = packageInfo.versionName;

				Toast.makeText(ComandaMarfa.this, getResources().getString(R.string.appVersion) + " " + versionName, Toast.LENGTH_LONG).show();

				String url = getString(R.string.serverVersion);

				NetworkGetRequest networkGetRequest = new NetworkGetRequest();
				networkGetRequest.run(url, new Callback(){
					@Override
					public void onFailure(Call call, IOException e){
						e.printStackTrace();
					}

					@Override
					public void onResponse(Call call, final Response response) throws IOException{
						if (! response.isSuccessful()){
							throw new IOException(getString(R.string.failure_code) + response);
						}else{

							final int serverVersionCode = Integer.parseInt(response.body().string());

							runOnUiThread(new Runnable(){
								@Override
								public void run(){
									if (versionCode < serverVersionCode){
										AlertDialog.Builder builder = new AlertDialog.Builder(ComandaMarfa.this).setTitle(getResources().getString(R.string.update_title)).setMessage(getResources().getString(R.string.update_message)).setCancelable(false).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
											@Override
											public void onClick(DialogInterface dialog, int which){
												dialog.dismiss();
											}
										}).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
											@Override
											public void onClick(DialogInterface dialog, int which){
												if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
													UpdateApp updater = new UpdateApp(ComandaMarfa.this);
													updater.execute();
												}else{
													Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.apkLink)));
													startActivity(intent);
												}
											}
										});

										AlertDialog dialog = builder.create();
										dialog.show();
									}
								}
							});
						}
					}
				});
			}catch (Exception e){
				e.printStackTrace();
				Log.e(TAG, e.getMessage());
			}

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
				if (checkPermission()){

				}else{
					requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
				}
			}

			String keyToStr = MA.getPreferencesString("key", null);
			String sediuToStr = MA.getPreferencesString("sediu", null);

			String from = MA.getFrom();

			if (keyToStr == null && sediuToStr == null && from.equals("")){
				Intent intent = new Intent(ComandaMarfa.this, MainActivity.class);
				startActivity(intent);
			}

			fisclaCode = findViewById(R.id.fiscalCode);
			clientName = findViewById(R.id.clientName);
			clientPhone = findViewById(R.id.phone);
			comment = findViewById(R.id.comment);
			nrDoc = findViewById(R.id.nrDoc);
			final ProgressDialog dialog = new ProgressDialog(ComandaMarfa.this);
			dialog.setMessage(getResources().getString(R.string.loading_data));
			dialog.setCancelable(false);
			dialog.show();
			MA.setFrom("");

			clientName.requestFocus();

			String api_token = MA.getPreferencesString("api_token", "");

			FormBody.Builder form = new FormBody.Builder();
			form.add("api_token", api_token);

			String url = getResources().getString(R.string.get_request_number);

			NetworkPostRequest networkPostRequest = new NetworkPostRequest();
			networkPostRequest.run(url, form.build(), new Callback(){
				@Override
				public void onFailure(Call call, IOException e){
					e.printStackTrace();
				}

				@Override
				public void onResponse(Call call, Response response) throws IOException{
					if (! response.isSuccessful()){
						throw new IOException(getString(R.string.failure_code) + response);
					}else{
						try{
							final JSONArray resp = new JSONArray(response.body().string());
							final JSONObject rsp = resp.getJSONObject(0);

							runOnUiThread(new Runnable(){
								@Override
								public void run(){
									try{
										String documentNumber = rsp.getString("NR_COMANDA_TABLETA");
										MA.setNrDoc(documentNumber);
										nrDoc.setText(documentNumber);
										if (dialog.isShowing()){
											dialog.dismiss();
										}

									}catch (JSONException e){
										e.printStackTrace();
									}
								}
							});

						}catch (JSONException e){
							e.printStackTrace();
						}
					}
				}
			});

			search.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					if (MA.isOnline()){
						//                if(clientName.getText().toString().isEmpty()){
						//                    AlertDialog.Builder dialog = new AlertDialog.Builder(ComandaMarfa.this);
						//                    dialog.setMessage("Introduceti numele clientului");
						//                    dialog.setCancelable(true);
						//                    dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
						//                        @Override
						//                        public void onClick(DialogInterface dialog, int which) {
						//
						//                        }
						//                    });
						//                    AlertDialog alert = dialog.create();
						//                    alert.show();
						//               } else {
						MA.setClientName(clientName.getText().toString());
						MA.setClientPhone(clientPhone.getText().toString());
						MA.setComment(comment.getText().toString());
						MA.setFiscalCode(fisclaCode.getText().toString());
						Intent intent = new Intent(ComandaMarfa.this, CautareMarfaActivity.class);
						startActivity(intent);
						//                }
					} else {
						Toast.makeText(ComandaMarfa.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
						startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
					}

				}
			});

			settings.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					if (MA.isOnline()){
						MA.setFrom("from");
						Intent intent = new Intent(ComandaMarfa.this, MainActivity.class);
						startActivity(intent);
					} else {
						Toast.makeText(ComandaMarfa.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
						startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
					}
				}
			});

			list.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					if (MA.isOnline()){
						Intent intent = new Intent(ComandaMarfa.this, OrdersListActivity.class);
						startActivity(intent);
					} else {
						Toast.makeText(ComandaMarfa.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
						startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
					}
				}
			});
			dialog.dismiss();
		} else {
			Toast.makeText(ComandaMarfa.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
			startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
		}
    }
    public static synchronized ComandaMarfa getAppInstance() {
        return appInstance;
    }

    private boolean checkPermission(){
        return (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE + Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission(String[] strings, int i) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode){
            case 1 :
                if(grantResults.length > 0){
                    boolean accept = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(accept){

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE + Manifest.permission.WRITE_EXTERNAL_STORAGE));
                            showMessageOKCancel(getResources().getString(R.string.need_permissions),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                            {
                                                requestPermission(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener)
    {
        new  android.support.v7.app.AlertDialog.Builder(ComandaMarfa.this)
                .setMessage(message)
                .setTitle(getResources().getString(R.string.permissions))
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
