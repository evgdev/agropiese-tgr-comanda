package com.example.agropiesetgrcomanda.Model;

public class SediiItemModel{
	private String sediu, sediuID;

	public SediiItemModel(String sediu, String sediuID){
		this.sediu = sediu;
		this.sediuID = sediuID;
	}

	public String getSediu(){
		return sediu;
	}

	public String getSediuID(){
		return sediuID;
	}
}
