package com.example.agropiesetgrcomanda.Model;

public class LanguageItem{
	private String language;

	public LanguageItem(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}
}
