package com.example.agropiesetgrcomanda.Model;

public class OrderListModel {

    String productDen, productCant, manager, orderDate, clientName, orderStatus, lastModOrder,orderID, codMarfa;

    public OrderListModel(String productDen, String productCant, String manager, String orderDate,
                          String clientName, String orderStatus, String lastModOrder, String orderID, String codMarfa) {
        this.productDen = productDen;
        this.productCant = productCant;
        this.manager = manager;
        this.orderDate = orderDate;
        this.clientName = clientName;
        this.orderStatus = orderStatus;
        this.lastModOrder = lastModOrder;
        this.orderID = orderID;
        this.codMarfa = codMarfa;
    }

    public String getProductDen() {
        return productDen;
    }

    public String getProductCant() {
        return productCant;
    }

    public String getManager() {
        return manager;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getClientName() {
        return clientName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getLastModOrder() {
        return lastModOrder;
    }

    public String getOrderID() {
        return orderID;
    }

    public String getCodMarfa(){
        return codMarfa;
    }

}
